At Shields Dental & Implant Clinic Limerick, patients are treated by the third generation of a family of distinguished dentists. Call us today at +353 61 480 070!

Address: 10 Ashdown Centre, Courtbrack Avenue, South Circular Road, Limerick, V94 K5TX, Ireland

Phone: +353 61 480 070